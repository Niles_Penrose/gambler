from aiogram.dispatcher import FSMContext
from aiogram.dispatcher.filters.state import State, StatesGroup



class Form(StatesGroup):
    main_menu = State()
    instruction = State()


class PlayForm(StatesGroup):
    play = State()
    set_bid = State()
    lobby_wait = State()

class WalletFormCommon(StatesGroup):
    wallet = State()

class WalletFormPay(StatesGroup):
    wallet = State()
    number = State()
    amount = State()
    Work_with_manager = State()