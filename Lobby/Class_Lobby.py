import typing
from aiogram import types
from loader import cb


class Lobby:
    total = 0

    def __init__(self, host_client_id, bid, lobbys_name):
        self.lobby_id = Lobby.total + 1
        Lobby.total += 1
        self.lobbys_name = lobbys_name
        self.host_pl = host_client_id
        self.invite_pl = 0
        self.bid = bid

    def IsEmpty(self):
        if self.invite_pl == 0:
            return True
        else:
            return False

def get_lob_by_host_id(list_of_lobbys: typing.List[Lobby], host_id: int):
    for each in list_of_lobbys:
        if each.host_pl == host_id:
            return each






def generate_list_of_opened_lobbys(list_of_lobbys: typing.List[Lobby]):
    keyboard = types.InlineKeyboardMarkup(True, True)
    if len(list_of_lobbys) < 1:
        return keyboard
    for each in list_of_lobbys:
        if each.IsEmpty():
            btn = types.InlineKeyboardButton(text = each.lobbys_name + ' ставка:' + str(each.bid),
                                             callback_data=cb.new(id=each.host_pl, action="join"))
            keyboard.add(btn)
    return keyboard


