from loader import dp, bot, IsAdmin
from aiogram import types


@dp.message_handler(IsAdmin())
async def only_for_admins(message: types.Message):
    await message.reply("Only for admins!")