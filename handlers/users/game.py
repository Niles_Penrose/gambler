from loader import dp, bot, IsAdmin, lobbys, cb
from aiogram import types
from aiogram.dispatcher import FSMContext
from format.keyboards import MAIN_MENU, WALLET, PLAY, WAITING
from format.messages import UNRECOGNIZED_MESSAGE, NOT_ENOUGH_PIP, BID_MSG
from format.utils import Form, PlayForm, WalletFormCommon, WalletFormPay
from Lobby.Class_Lobby import Lobby, get_lob_by_host_id, generate_list_of_opened_lobbys


@dp.message_handler(state='*', commands=['start'])
async def process_start_command(msg: types.Message):

    # todo check for new user
    # todo work with local storage
    await Form.main_menu.set()
    await msg.answer("hello there", reply_markup=MAIN_MENU)


@dp.message_handler(state=Form.main_menu)
async def process_main_menu(msg: types.Message, state: FSMContext):
    if msg.text == "играть":
        # todo list of opened lobbys
        kb = generate_list_of_opened_lobbys(lobbys)
        await msg.answer("подключитесь к одному из открытых лобби или создайте своё", reply_markup=PLAY)
        await msg.answer("доступные лобби:", reply_markup=kb)
        await PlayForm.play.set()

    elif msg.text == "инструкция":
        await msg.answer("инструкция. большая, пишет менеджер.")

    elif msg.text == "кошелёк":
        await msg.answer(msg.text, reply_markup=WALLET)
        await WalletFormCommon.wallet.set()

    else:
        await msg.answer(UNRECOGNIZED_MESSAGE, reply_markup=MAIN_MENU)


@dp.message_handler(state=PlayForm.play)
async def process_play(msg: types.Message, state: FSMContext):
    if msg.text == "главное меню":
        await Form.main_menu.set()
        await msg.answer("hello there", reply_markup=MAIN_MENU)


    elif msg.text == "создать игру":
        # todo check_pipcoin_balance() return bool
        await msg.answer("check_pipcoin_balance")
        st = True

        if st == False:
            await msg.answer(NOT_ENOUGH_PIP, reply_markup=WALLET)
            await WalletFormCommon.wallet.set()
        else:
            await PlayForm.next()
            await msg.answer(BID_MSG, reply_markup=types.ReplyKeyboardRemove())

@dp.callback_query_handler(cb.filter(action = ["join"]), state='*')
async def some_shit(call: types.CallbackQuery, callback_data: dict):
    await call.answer("join")

    lobby_host_id = int(callback_data["id"])
    lob = get_lob_by_host_id(lobbys, lobby_host_id)
    lob.invite_pl = call.from_user.id
    # lobbys[lobby_id] = lob
    # todo work with staroge
    await bot.send_message(chat_id=call.from_user.id, text="вы присоединились к лобби " + lob.lobbys_name)
@dp.message_handler(state=PlayForm.set_bid)
async def process_play(msg: types.Message, state: FSMContext):
    bid = 0
    try:
        bid = int(msg.text)
    except:
        await msg.answer("введите ставку (только цифры)", reply_markup=types.ReplyKeyboardRemove())
        return -1

    if bid > 3 and bid <= 1000:
        await msg.answer(f"вы в игре, ставка = {bid}\n")

        lob = Lobby(msg.chat.id, bid, msg.chat.first_name)
        lobbys.append(lob) # create lobby
        # todo set data(bid)
        await PlayForm.next()
        await msg.answer("ждём пока к вам кто-нибудь подключиться", reply_markup=WAITING)
    else:
        await msg.answer(BID_MSG)
        return -1



@dp.message_handler(state=PlayForm.lobby_wait)
async def process_play(msg: types.Message, state: FSMContext):
    if msg.text == "покинуть лобби":
        lobby = get_lob_by_host_id(lobbys, msg.chat.id)
        lobbys.remove(lobby)  # delete lobby
        await Form.main_menu.set()
        await msg.answer("hello there", reply_markup=MAIN_MENU)
        pass
    elif msg.text == "изменить ставку":
        await PlayForm.set_bid.set()
        await msg.answer(BID_MSG, reply_markup=types.ReplyKeyboardRemove())



