from loader import dp, bot, IsAdmin
from config import manager
from aiogram import types
from aiogram.dispatcher import FSMContext
from format.keyboards import MAIN_MENU, WALLET, PLAY, WAITING
from format.messages import UNRECOGNIZED_MESSAGE, NOT_ENOUGH_PIP, BID_MSG, GET_CASH, OFFER_ON_PAY
from format.utils import Form, PlayForm, WalletFormCommon, WalletFormPay


@dp.message_handler(state=WalletFormCommon.wallet)
async def process_main_menu(msg: types.Message, state: FSMContext):
    if msg.text == "вывести":
        await msg.answer(GET_CASH, reply_markup=types.ReplyKeyboardRemove())
        await WalletFormPay.wallet.set()


    elif msg.text == "пополнить":
        await msg.answer("QIWI WALLET IS NOT CONNECTED")

    elif msg.text == "главное меню":
        await Form.main_menu.set()
        await msg.answer("hello there", reply_markup=MAIN_MENU)


@dp.message_handler(state=WalletFormPay.wallet)
async def process_main_menu(msg: types.Message, state: FSMContext):
    number = msg.text
    # todo check the number
    async with state.proxy() as data:
        data['number'] = number

    await WalletFormPay.next()
    await msg.answer("теперь сумму, которую вы хотите вывести(только цифры)")


@dp.message_handler(state=WalletFormPay.number)
async def process_main_menu(msg: types.Message, state: FSMContext):
    amount = 0
    try:
        amount = int(msg.text)
    except:
        await msg.answer("введите сумму(только цифры)")
        return -1

    await state.update_data(amount=int(msg.text))

    info = await state.get_data()

    print(info)
    # todo change status in database
    await bot.send_message(manager, OFFER_ON_PAY + f"номер: {info['number']}\nсумма: {info['amount']}")

    await msg.answer("если возникли какие-то проблемы пишите на этот контакт @wewq", reply_markup=MAIN_MENU)
    await state.finish()
    await Form.main_menu.set()
