from .game import dp
from .wallet import dp
from .admin.manager_command import dp
from .default import dp
__all__ = [dp]