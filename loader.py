import logging
import asyncio
import typing
from aiogram.dispatcher import Dispatcher
from aiogram import Bot, types
from config import BOT_TOKEN, ADMINS
from aiogram.contrib.fsm_storage.memory import MemoryStorage
from aiogram.contrib.fsm_storage.redis import RedisStorage2
from aiogram.contrib.middlewares.logging import LoggingMiddleware
from aiogram.dispatcher.filters import Filter
from aiogram.utils.callback_data import CallbackData

logging.basicConfig(format=u'%(filename)+13s [ LINE:%(lineno)-4s] %(levelname)-8s [%(asctime)s] %(message)s',
                    level=logging.INFO)
loop = asyncio.get_event_loop()

bot = Bot(token=BOT_TOKEN)
storage = MemoryStorage()
# storage = RedisStorage2('localhost', 6379, db=5, pool_size=10, prefix='my_fsm_key')

lobbys = []
# todo проверка что лобби от игрока только одно

dp = Dispatcher(bot=bot, loop=loop, storage=storage)
dp.middleware.setup(LoggingMiddleware())



cb = CallbackData("post", "id", "action")

class IsAdmin(Filter):
    key = "is_admin"

    async def check(self, message: types.Message):
        return message.from_user.id in ADMINS


dp.bind_filter(IsAdmin)